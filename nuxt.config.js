import colors from 'vuetify/es5/util/colors'
import pkg from './package'
require('dotenv').config()
export default {
    mode: 'spa',
    /*
     ** Headers of the page
     */
    head: {
        title: 'Cẩm nang game thủ ĐTCL',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'stylesheet',
                href: 'https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css'
            }
        ]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: {
        color: 'blue',
        height: '2px'
    },
    /*
     ** Global CSS
     */
    css: ['~/assets/css/style1.css', '~/assets/css/frontend.css'],

    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '~/plugins/main',
        // '~/plugins/icons',
        '~/plugins/axios',
        '~/plugins/sweetalert',
        '~/plugins/i18n',
        // '~/plugins/vuetify',
        '~/plugins/vee-validate',
        { src: '~/plugins/vue2-editor', mode: 'client' },
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: ['@nuxtjs/vuetify'],
    /*
     ** Nuxt.js modules
     */
    modules: [
        '@nuxtjs/axios',
        'cookie-universal-nuxt'
    ],

    axios: {
        // See https://github.com/nuxt-community/axios-module#options
        // proxy: true,
        baseURL: 'http://localhost:8089/backend_web_dtcl/public/api/',
        // proxyHeaders: false,
        // credentials: true,
    },

    router: {
        middleware: ['i18n'],
    },

    vuetify: {
        customVariables: ['~/assets/variables.scss'],
        theme: {
            light: true,
            themes: {
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3
                }
            }
        }
    },
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */


        transpile: ['vee-validate/dist/rules'],

        vendor: ['vue-i18n'],
        babel: {
            presets({ isServer }) {
                return [
                    [
                        require.resolve('@nuxt/babel-preset-app'),
                        // require.resolve('@nuxt/babel-preset-app-edge'), // For nuxt-edge users
                        {
                            buildTarget: isServer ? 'server' : 'client',
                        }
                    ]
                ]
            }
        },
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {
            config.node = {
                fs: 'empty'
            }
        }
    }
}
