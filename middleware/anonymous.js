export default function({ store, redirect, app, req }) {
    store.commit('admin/auth/SET_LOGIN', null !== localStorage.getItem('auth'))
    if (store.getters['admin/auth/isLogined'] === true) {
        return redirect('/admin')
    }
}