export default function({ isHMR, app, store, route, params, req, error, redirect }) {
    if (isHMR) { // ignore if called from hot module replacement
        return;
    }
    if (req) {
        if (route.name) {
            let locale = null;
            if (process.server) {
                locale = localStorage.getItem('locale')
            }

            store.commit('SET_LANG', locale);
            app.i18n.locale = store.state.locale;
        }
    }
};
