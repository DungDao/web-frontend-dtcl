export default function({ store, redirect, app, req }) {
    store.commit('admin/auth/SET_LOGIN_HOME', null !== localStorage.getItem('auth_home'))
    if (store.getters['admin/auth/isLoginedHome'] === true) {
        return redirect('/posts')
    }
}