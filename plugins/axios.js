export default function({ $axios, store, redirect, app }) {
    $axios.onRequest(config => {
        config.headers.common['Authorization'] =
            'Bearer ' + localStorage.getItem('auth')
        config.headers.common['Accept'] = 'application/json'
    })

    $axios.onResponse(response => {
        if (response.data.message == 'Đăng nhập để được phép truy cập' && response.data.code == '500') {
            store.commit('auth/SET_LOGIN', false);
            return redirect('/login');
        }
    })

}