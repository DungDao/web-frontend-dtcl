import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'
Vue.use(VueLocalStorage);
import Notifications from 'vue-notification'

Vue.use(Notifications)
Vue.use(require('vue-moment'))
const moment = require('moment')

Vue.prototype.$formatDate = (dateString, format) =>
    moment(dateString).format(format)

Vue.prototype.$createDateObjectFromUnixNumber = dateNumber => moment(dateNumber)

Vue.prototype.$isValidDate = dateString => moment(dateString).isValid()

Vue.prototype.$fromNow = dateString => moment(dateString).fromNow()

Vue.prototype.$isNowBetween = (startDate, endDate) =>
    moment().isBetween(startDate, endDate, 'day', '[]')

Vue.prototype.$startOfISOWeek = format =>
    moment()
    .startOf('isoWeek')
    .format(format)

Vue.prototype.$today = format => moment().format(format)

// common functions
Vue.prototype.$getDateObject = date =>
    date ? Vue.prototype.$createDateObjectFromUnixNumber(date) : null

Vue.prototype.$saveLoginUserInformation = (user, isLogin = true) => {
    localStorage.setItem('email', user.data.email)
    localStorage.setItem('user', user.data.name)
    localStorage.setItem('roles', user.data.roles)
    localStorage.setItem('userData', JSON.stringify(user.data))
    localStorage.setItem('remeber', user.remeber)
    localStorage.setItem('password', user.password);

    isLogin && localStorage.setItem('auth', user.access_token)
}

Vue.prototype.$saveLoginHomeUserInformation = (user, isLoginHome = true) => {
    localStorage.setItem('email_user', user.data.email)
    localStorage.setItem('user_home', user.data.name)
    localStorage.setItem('roles_user', user.data.roles)
    localStorage.setItem('userData1', JSON.stringify(user.data))
    localStorage.setItem('remeber_user', user.remeber)
    localStorage.setItem('password_user', user.password);
    isLoginHome && localStorage.setItem('auth_home', user.access_token)
}

Vue.prototype.$promotionDateFormat = 'YYYY-MM-DD'

Vue.prototype.$showErrorImage = function(event) {
    event.target.src = '/empty.png'
}
Vue.prototype.$uploadImage = async function(files) {
    if (null !== files) {
        let fileFormData = new FormData()
        fileFormData.append('image', files)
        let response = await this.$store.dispatch('admin/common/upload', fileFormData)
        if (200 === response.code) {
            return response.data
        } else {
            this.$showError(response.message)

            return ''
        }
    } else {
        return ''
    }
}

Vue.prototype.$formatPrice = function(value) {
    let val = (value / 1).toFixed(0).replace('.', ',')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}

Vue.prototype.$getAvatarUrl = function(avatarUrl) {
    if (avatarUrl) {
        const pattern = new RegExp(
                '^(https?:\\/\\/)?' + // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$',
                'i'
            ) // fragment locator

        return !!pattern.test(avatarUrl) ?
            avatarUrl :
            'http://localhost:8080/a_laravel/backend_web_dtcl/public/uploads/images/' + avatarUrl
    } else {
        return '/img/avatar-blank.jpg'
    }
}

Vue.prototype.$getFileUrl = function(fileUrl) {
    return 'http://localhost:8089/backend_web_dtcl/public/uploads/images/' + fileUrl
}

Vue.prototype.$showError = function(message, text) {
    swal({
        title: text ? text : 'Lỗi',
        text: message,
        icon: 'error',
        timer: 3000
    })
}

Vue.prototype.$showSuccess = function(message, text) {
    swal({
        title: message,
        text: text ? text : '',
        icon: 'success',
        timer: 3000
    })
}
Vue.prototype.$changeText = function(text) {
    return text.toUpperCase().normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '').replace(/\s+/g, '')
        .replace(/đ/g, 'd').replace(/Đ/g, 'D');
}

Vue.prototype.$getStatusColor = function(status) {
    return 1 === status ? 'success' : 'blue-grey'
}

Vue.prototype.$getStatusIcon = function(status) {
    return 1 === status ? 'mdi-lock-open-outline' : 'mdi-lock'
}

Vue.prototype.$uploadImageEditor = async function(files) {
    if (null !== files) {
        let fileFormData = new FormData()
        fileFormData.append('image', files)
        let response = await this.$store.dispatch('admin/common/uploadEditor', fileFormData)
        if (200 === response.code) {
            return response.data
        } else {
            this.$showError(response.message)

            return ''
        }
    } else {
        return ''
    }
}

Vue.prototype.$perPageItems = [5, 10, 15]

Vue.prototype.$perPage = 5

Vue.prototype.$statuses = { 1: 'active', 2: 'inactive' }
    //Policy development


Vue.prototype.$classTypes = [
    { name: 'Hệ', value: 0 },
    { name: 'Tộc', value: 1 }
]
Vue.prototype.$rateds = [
    { name: 'S', value: 's' },
    { name: 'A', value: 'a' },
    { name: 'B', value: 'b' },
    { name: 'C', value: 'c' },
    { name: 'D', value: 'd' },
]


Vue.prototype.$checkStatus = function(status) {
    let strResult = ''
    switch (status) {
        case 'buff':
            strResult += 'Tăng Sức Mạnh';
            break;
        case 'nerf':
            strResult += 'Giảm Sức Mạnh';
            break;
        default:
            strResult += "Điều Chỉnh";

    }
    return strResult;
}

Vue.prototype.$usesTypes = [
    { name: ' Sức mạnh công kích', value: 'attack_damage' },
    { name: '% Tốc độ đánh', value: 'attack_speed' },
    { name: ' Giáp', value: 'armor' },
    { name: ' Kháng phép', value: 'magic_resist' },
    { name: '% Sát thương kỹ năng', value: 'spell_damage' },
    { name: ' Năng lượng khởi đầu', value: 'starting_mana' },
    { name: ' Máu', value: 'health' },
    { name: ' Nó phải có tác dụng gì đó', value: 'special' },
    { name: '% Tỉ lệ chí mạng & Tỉ lệ né', value: 'gloves' },
]
Vue.prototype.$statusChampions = [
    { name: 'Tăng Sức Mạnh', value: 'buff' },
    { name: 'Giảm Sức Mạnh', value: 'nerf' },
    { name: 'Điều Chỉnh', value: 'adjust' },
    { name: 'Không Thay Đổi', value: 'normal' },
]


Vue.prototype.$colorTitlePage = "background-color:gray"

Vue.prototype.$capitalizeString = string =>
    undefined !== string && string[0] ?
    string[0].toUpperCase() + string.slice(1) :
    null

Vue.prototype.$isNotEmptyObject = obj =>
    undefined !== obj &&
    null !== obj &&
    Object.keys(obj).length > 0 &&
    obj.constructor === Object
