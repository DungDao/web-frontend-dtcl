export default {
    SET_LIST_USER(state, payload) {
        state.listUserByDate = payload
    },

    SET_LIST_POST(state, payload) {
        state.listPostByDate = payload
    },
}