export default {
    getTotalHome({ commit }, payload) {
        return this.$axios.$get('admin/home/get-total-home').then(res => {
            return res;
        }).catch(err => {
            return err;
        })
    },

    getListUserByDate({ commit }, payload) {
        this.$axios.$get('admin/home/get-number-user-by-date').then(res => {
            if (res.code == 200) {
                commit('SET_LIST_USER', res.data);
            }
        }).catch(err => {
            return err;
        })
    },

    getListPostByDate({ commit }, payload) {
        this.$axios.$get('admin/home/get-number-post-by-date').then(res => {
            if (res.code == 200) {
                commit('SET_LIST_POST', res.data);
            }
        }).catch(err => {
            return err;
        })
    },
}
