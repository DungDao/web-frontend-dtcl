export default {
    statusEditModal: state => state.statusEditModal,
    listRate: state => state.listRate,
    editRate: state => state.editRate,
}