export default {
    loadList({ commit }, payload) {
        let url = "admin/combination-rate";
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    add({ commit, dispatch }, payload) {
        let url = "admin/combination-rate"
        return this.$axios
            .$post(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    delete({ commit, dispatch }, payload) {

        var url = 'admin/combination-rate/' + payload.id;
        return this.$axios
            .$delete(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    update({ commit }, payload) {
        var url = 'admin/combination-rate/' + payload.form.id
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
}