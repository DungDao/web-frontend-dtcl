export default {
    SET_STATUS_EDIT_MODAL(state, payload) {
        state.statusEditModal = payload
    },
    SET_EDIT_CLASSES(state, payload) {
        state.editClasses = payload
    },
    SET_ALL_CLASSES(state, payload) {
        state.allClasses = payload
    }

}