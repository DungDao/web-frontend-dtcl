export default {
    loadList({ commit }, payload) {
        let url = "admin/classes";
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    add({ commit, dispatch }, payload) {
        let url = "admin/classes"
        return this.$axios
            .$post(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    delete({ commit, dispatch }, payload) {

        var url = 'admin/classes/' + payload.id;
        return this.$axios
            .$delete(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    update({ commit }, payload) {
        var url = 'admin/classes/' + payload.form.id
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    loadAllClasses({ commit }, payload) {
        return this.$axios.$get('admin/class/get-all-classes').then(res => {
            commit("SET_ALL_CLASSES", res.data);
        }).catch(err => {
            return err;
        })
    },
}