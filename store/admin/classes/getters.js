export default {
    statusEditModal: state => state.statusEditModal,
    listClasses: state => state.listClasses,
    editClasses: state => state.editClasses,
    allClasses: state => state.allClasses,
}