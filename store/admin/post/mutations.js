export default {
    SET_STATUS_EDIT_MODAL(state, payload) {
        state.statusEditModal = payload
    },

    SET_STATUS_EDIT_MODAL1(state, payload) {
        state.statusEditModal1 = payload
    },
    SET_EDIT_CATEGORY(state, payload) {
        state.editCategory = payload
    },
    SET_ALL_CATEGORY(state, payload) {
        state.allCategory = payload
    },

    SET_EDIT_POST(state, payload) {
        state.editPost = payload
    },
    SET_ALL_POST(state, payload) {
        state.allPost = payload
    }

}