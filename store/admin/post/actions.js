export default {
    loadList({ commit }, payload) {
        let url = "admin/";
        if (payload.type == 'post-category') {
            url += 'post-category'
        } else {
            url += 'post'
        }
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    add({ commit, dispatch }, payload) {
        let url = "admin/";
        if (payload.type == 'post-category') {
            url += 'post-category'
        } else {
            url += 'post'
        }
        return this.$axios
            .$post(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    delete({ commit, dispatch }, payload) {
        let url = "admin/";
        if (payload.type == 'post-category') {
            url += 'post-category/' + payload.id;
        } else {
            url += 'post/' + payload.id;
        }
        return this.$axios
            .$delete(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    update({ commit }, payload) {
        let url = "admin/";
        if (payload.type == 'post-category') {
            url += 'post-category/' + payload.form.id;
        } else {
            url += 'post/' + payload.form.id;
        }
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    loadAllItem({ commit }, payload) {
        let url = 'admin/';
        if (payload.type == 'post-category') {
            url += 'category-post-all'
        } else {
            url += 'post-all'
        }
        return this.$axios.$get(url).then(res => {
            if (payload.type == 'post-category') {
                commit("SET_ALL_CATEGORY", res.data);
            } else {
                commit("SET_ALL_POST", res.data);
            }

        }).catch(err => {
            return err;
        })
    },

    posted({ commit }, payload) {
        let url = "admin/posts/posted/" + payload.form.id;
        return this.$axios.$put(url, payload.form).then(res => {
            return res;
        }).catch(err => {
            return err;
        })

    },
    loadListByCategory({ commit }, payload) {
        let url = "admin/get-posts-by-category/" + payload.id;
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
}