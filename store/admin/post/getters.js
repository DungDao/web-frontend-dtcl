export default {
    statusEditModal: state => state.statusEditModal,
    statusEditModal1: state => state.statusEditModal1,
    editCategory: state => state.editCategory,
    allCategory: state => state.allCategory,

    editPost: state => state.editPost,
    allPost: state => state.allPost,
}