export default {
    loadList({ commit }, payload) {
        let url = "admin/user/get-users";
        return this.$axios
            .$get(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    blockUser({ commit }, payload) {
        var url = 'admin/user/block-status/' + payload.form.id
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    loadAllClasses({ commit }, payload) {
        return this.$axios.$get('admin/class/get-all-classes').then(res => {
            commit("SET_ALL_CLASSES", res.data);
        }).catch(err => {
            return err;
        })
    },
}