export default {
    SET_STATUS_EDIT_MODAL(state, payload) {
        state.statusEditModal = payload
    },
    SET_EDIT_USER(state, payload) {
        state.editUser = payload
    },
}