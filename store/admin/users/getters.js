export default {
    statusEditModal: state => state.statusEditModal,
    editUser: state => state.editUser,
}