export default {
    statusEditModal: state => state.statusEditModal,
    listChampions: state => state.listChampions,
    editChampions: state => state.editChampions,
    allChampions: state => state.allChampions,
}