export default {
    SET_STATUS_EDIT_MODAL(state, payload) {
        state.statusEditModal = payload
    },
    SET_EDIT_CHAMPIONS(state, payload) {
        state.editChampions = payload
    },
    SET_ALL_CHAMPIONS(state, payload) {
        state.allChampions = payload
    }

}