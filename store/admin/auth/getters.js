export default {
    getUser: state => state.user,
    isLogined: state => state.isLogined,
    isLoginedHome: state => state.isLoginedHome,
    statusModal: state => state.statusModal,
    nameUserHome: state => state.nameUserHome,
}