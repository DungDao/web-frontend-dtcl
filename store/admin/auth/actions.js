export default {
    register({ commit, dispatch, rootState }, payload) {
        return this.$axios
            .post('auth/signup', payload)
            .then(function(response) {
                return response
            })
            .catch(error => {
                return error.response
            })
    },
    login({ commit, dispatch }, payload) {
        return this.$axios
            .post('auth/login', payload)
            .then(response => {
                return response
            })
            .catch(error => {
                return error.response
            })
    },

    loginHome({ commit, dispatch }, payload) {
        return this.$axios
            .post('auth/login-home', payload)
            .then(response => {
                return response
            })
            .catch(error => {
                return error.response
            })
    },
    // changPassword({ commit, dispatch }, payload) {
    //     return this.$axios.$patch("admin/auth/password", payload).then(res => {
    //         return res
    //     }).catch(error => {
    //         return error.res
    //     })
    // },
    // forgotPassword({ commit, dispatch }, payload) {
    //     return this.$axios
    //         .post('admin/auth/password/forgot', payload)
    //         .then(response => {
    //             return response
    //         })
    //         .catch(error => {
    //             return error.response
    //         })
    // },

    // resetPassword({ commit, dispatch }, payload) {
    //     return this.$axios
    //         .post('admin/auth/password/reset', payload.form)
    //         .then(response => {
    //             return response
    //         })
    //         .catch(error => {
    //             return error.response
    //         })
    // },
}