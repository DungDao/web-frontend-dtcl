export default () => ({
    isLogined: false,
    isLoginedHome: false,
    user: {
        email: '',
        password: '',
    },
    statusModal: null,

    nameUserHome: null,
})