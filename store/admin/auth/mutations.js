import swal from "sweetalert";

export default {
    UPDATE_USER_INPUT(state, user) {
        Object.assign(state.user, user);
    },
    SET_LOGIN(state, payload) {
        state.isLogined = payload;
    },

    SET_LOGIN_HOME(state, payload) {
        state.isLoginedHome = payload;
    },

    SET_STATUS_EDIT_MODAL(state, payload) {
        state.statusModal = payload;

    },

    SET_NAME_USER_HOME(state, payload) {
        state.nameUserHome = payload;
    }
}