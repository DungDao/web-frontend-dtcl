export default {
    statusEditModal: state => state.statusEditModal,
    listTeams: state => state.listTeams,
    editTeams: state => state.editTeams,
    allTeams: state => state.allTeams,
}