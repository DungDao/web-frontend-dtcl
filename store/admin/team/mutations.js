export default {
    SET_STATUS_EDIT_MODAL(state, payload) {
        state.statusEditModal = payload
    },
    SET_EDIT_TEAMS(state, payload) {
        state.editTeams = payload
    },
    SET_ALL_TEAMS(state, payload) {
        state.allTeams = payload
    }

}