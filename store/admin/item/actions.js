export default {
    loadList({ commit }, payload) {
        let url = "admin/";
        if (payload.type == 'base-item') {
            url += 'base-item'
        } else {
            url += 'advanced-item'
        }
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    add({ commit, dispatch }, payload) {
        let url = "admin/";
        if (payload.type == 'base-item') {
            url += 'base-item'
        } else {
            url += 'advanced-item'
        }
        return this.$axios
            .$post(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    delete({ commit, dispatch }, payload) {
        let url = "admin/";
        if (payload.type == 'base-item') {
            url += 'base-item/' + payload.id;
        } else {
            url += 'advanced-item/' + payload.id;
        }
        return this.$axios
            .$delete(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    update({ commit }, payload) {
        let url = "admin/";
        if (payload.type == 'base-item') {
            url += 'base-item/' + payload.form.id;
        } else {
            url += 'advanced-item/' + payload.form.id;
        }
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    loadAllItem({ commit }, payload) {
        let url = 'admin/';
        if (payload.type == 'base-item') {
            url += 'base-item-all'
        } else {
            url += 'advanced-item-all'
        }
        return this.$axios.$get(url).then(res => {
            if (payload.type == 'base-item') {
                commit("SET_ALL_BASE_ITEM", res.data);
            } else {
                commit("SET_ALL_ADVANCED_ITEM", res.data);
            }

        }).catch(err => {
            return err;
        })
    },
    //base item of advanced item table 
    addItem({ commit, dispatch }, payload) {
        let url = "admin/base-of-advanced-item"
        return this.$axios
            .$post(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    deleteItem({ commit, dispatch }, payload) {

        var url = 'admin/base-of-advanced-item/' + payload.id;
        return this.$axios
            .$delete(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    updateItem({ commit }, payload) {
        var url = 'admin/base-of-advanced-item/' + payload.form.id
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    getItemById({ commit }, payload) {
        var url = "admin/advanced-item/" + payload.id;
        return this.$axios.$get(url).then(res => {
            return res
        }).catch(error => {
            return error
        })
    }
}