export default {
    SET_STATUS_EDIT_MODAL(state, payload) {
        state.statusEditModal = payload
    },

    SET_STATUS_EDIT_MODAL1(state, payload) {
        state.statusEditModal1 = payload
    },
    SET_EDIT_BASE_ITEM(state, payload) {
        state.editBaseItem = payload
    },
    SET_ALL_BASE_ITEM(state, payload) {
        state.allBaseItems = payload
    },

    SET_EDIT_ADVANCED_ITEM(state, payload) {
        state.editAdvancedItem = payload
    },
    SET_ALL_ADVANCED_ITEM(state, payload) {
        state.allAdvancedItems = payload
    }

}