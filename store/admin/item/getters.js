export default {
    statusEditModal: state => state.statusEditModal,
    statusEditModal1: state => state.statusEditModal1,
    editBaseItem: state => state.editBaseItem,
    allBaseItems: state => state.allBaseItems,

    editAdvancedItem: state => state.editAdvancedItem,
    allAdvancedItems: state => state.allAdvancedItems,
}