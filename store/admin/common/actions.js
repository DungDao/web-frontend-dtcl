export default {
    upload({ commit, dispatch, rootState }, payload) {
        return this.$axios
            .post('common/upload', payload)
            .then(function(response) {
                return response.data
            })
            .catch(error => {
                return error.response
            })
    },
    uploadEditor({ commit, dispatch, rootState }, payload) {
        return this.$axios
            .post('common/upload-image-editor', payload)
            .then(function(response) {
                return response.data
            })
            .catch(error => {
                return error.response
            })
    }
}
