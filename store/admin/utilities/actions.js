export default {
    loadList({ commit }, payload) {
        let url = "admin/";
        if (payload.type == 'rolling') {
            url += 'rolling-odd'
        } else {
            url += 'utilities'
        }
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    add({ commit, dispatch }, payload) {
        let url = "admin/";
        if (payload.type == 'rolling') {
            url += 'rolling-odd'
        } else {
            url += 'utilities'
        }
        return this.$axios
            .$post(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    delete({ commit, dispatch }, payload) {
        let url = "admin/";
        if (payload.type == 'rolling') {
            url += 'rolling-odd/' + payload.id;
        } else {
            url += 'utilities/' + payload.id;
        }
        return this.$axios
            .$delete(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    update({ commit }, payload) {
        let url = "admin/";
        if (payload.type == 'rolling') {
            url += 'rolling-odd/' + payload.form.id;
        } else {
            url += 'utilities/' + payload.form.id;
        }
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    getByTypeUtility({ commit }, payload) {
        let url = "admin/utility/get-by-type";
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })

    }
}