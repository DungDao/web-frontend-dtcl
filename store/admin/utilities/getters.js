export default {
    statusEditModal: state => state.statusEditModal,
    statusEditModal1: state => state.statusEditModal1,
    editRolling: state => state.editRolling,
    allRollings: state => state.allRollings,

    editUtility: state => state.editUtility,
    allUtilities: state => state.allUtilities,
}