export default {
    SET_STATUS_EDIT_MODAL(state, payload) {
        state.statusEditModal = payload
    },

    SET_STATUS_EDIT_MODAL1(state, payload) {
        state.statusEditModal1 = payload
    },
    SET_EDIT_ROLLING(state, payload) {
        state.editRolling = payload
    },
    SET_ALL_ROLLING(state, payload) {
        state.allRollings = payload
    },

    SET_EDIT_UTILITY(state, payload) {
        state.editUtility = payload
    },
    SET_ALL_UTILITIES(state, payload) {
        state.allUtilities = payload
    }

}