export const strict = false
export const state = () => ({
    locales: [{
            code: 'vn',
            name: 'VN'
        },
        {
            code: 'en',
            name: 'EN'
        },
    ],
    locale: 'vn'
});

export const mutations = {
    SET_LANG(state, locale) {
        if (state.locales.find(el => el.code === locale)) {
            state.locale = locale
        }
    }
}
