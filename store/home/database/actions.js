export default {
    loadList({ commit }, payload) {
        let url = 'home/';
        let urlCommit = null;
        switch (payload.type) {
            case 'champions':
                url += 'list-champions';
                urlCommit = 'GET_LIST_CHAMPION';
                break;
            case 'items':
                url += 'list-items'
                urlCommit = 'GET_LIST_ITEM';
                break;
            case 'base_item':
                url += 'list-base-items'
                urlCommit = 'GET_LIST_BASE_ITEM'
                break;
            case 'classes':
                url += 'list-classes';
                urlCommit = 'GET_LIST_CLASSES'
                break;
            default:
                url += 'get-teams'
                urlCommit = 'GET_LIST_TEAM'
        }
        this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                if (res.code == 200) {
                    commit(urlCommit, res.data);
                }
            })
            .catch(error => {
                return error
            })
    },

    getChampions({ commit }, payload) {
        let url = "home/list-champions";
        return this.$axios.$get(url, { params: payload.params })
            .then(res => {
                return res;
            })
            .catch(error => {
                return error
            })
    },

    getChampionById({ commit }, payload) {
        let url = "home/champions/" + payload.id;
        return this.$axios.$get(url).then(res => {
            return res;
        }).catch(err => {
            return err;
        })
    },

    getAllItems({ commit }, payload) {
        let url = "home/list-items";
        return this.$axios.$get(url).then(res => {
            return res;
        }).catch(err => {
            return err;
        })
    },

    getAllItemWithSearch({ commit }, payload) {
        let url = "home/list-items";
        return this.$axios.$get(url, { params: payload.params }).then(res => {
            return res;
        }).catch(err => {
            return err;
        })
    },

    getByTypeUtility({ commit }, payload) {
        let url = "home/get-utilities-by-type";
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })

    },

    getClassWithChampions({ commit }, payload) {
        let url = "home/list-classes-with-champions";
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                if (res.code == 200) {
                    commit('GET_LIST_CLASS_WITH_CHAMPIONS', res.data);
                }
            })
            .catch(error => {
                return error
            })

    },

    loadRolling({ commit }, payload) {
        let url = "home/get-rollings";
        return this.$axios
            .$get(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },


    // User info----------------------------------------------------------------------------------------------
    getUser({ commit }, payload) {
        let url = "home/get-user-by-id/" + payload.id;
        return this.$axios.$get(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    updateInfoUser({ commit }, payload) {
        var url = 'home/change-info-user/' + payload.form.id
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    confirmEmail({ commit }, payload) {
        var url = "auth/reset-password";
        return this.$axios
            .$post(url, payload)
            .then(res => {
                return res
            }).catch(error => {
                return error;
            })
    },

    resetPassword({ commit }, payload) {
        var url = "auth/reset-password/" + payload.token;
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            }).catch(error => {
                return error;
            })
    }


}