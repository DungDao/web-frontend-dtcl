export default {
    listChampions: state => state.listChampions,
    listItems: state => state.listItems,
    listBaseItems: state => state.listBaseItems,
    listClasses: state => state.listClasses,
    listTeams: state => state.listTeams,
    listClassWithChampions: state => state.listClassWithChampions,
}