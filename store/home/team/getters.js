export default {
    listTeams: state => state.listTeams,
    editTeams: state => state.editTeams,
    listChampions: state => state.listChampions,
}