export default {
    getTeamByUserId({ commit }, payload) {
        let url = "home/team-by-user/" + payload.id;
        return this.$axios.$get(url, { params: payload.params }).then(res => {
            if (res.code == 200) {
                commit('GET_LIST_TEAMS', res.data);
            }
        }).catch(err => {
            return err;
        })
    },

    getChampions({ commit }, payload) {
        let url = "home/all-champions";
        return this.$axios.get(url, { params: payload.params }).then(res => {
            return res.data;
        }).catch(err => {
            return err;
        })
    },

    addTeam({ commit, dispatch }, payload) {
        let url = "home/team"
        return this.$axios
            .$post(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    deleteTeam({ commit, dispatch }, payload) {

        var url = 'home/team/' + payload.id;
        return this.$axios
            .$delete(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    updateTeam({ commit }, payload) {
        var url = 'home/team/' + payload.form.id
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    getTeamById({ commit }, payload) {
        var url = "home/team/" + payload.id;
        return this.$axios.$get(url).then(res => {
            return res;
        }).catch(error => {
            return error
        })
    },


}