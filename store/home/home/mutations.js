export default {
    GET_LIST_CHAMPION(state, payload) {
        state.listChampions = payload
    },

    GET_LIST_ITEM(state, payload) {
        state.listItems = payload
    },

    GET_LIST_BASE_ITEM(state, payload) {
        state.listBaseItems = payload
    },

    GET_LIST_CLASSES(state, payload) {
        state.listClasses = payload
    },

    GET_LIST_TEAM(state, payload) {
        state.listTeams = payload
    },

}