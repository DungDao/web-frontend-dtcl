export default () => ({
    listChampions: null,
    listItems: null,
    listBaseItems: null,
    listClasses: null,
    listTeams: null,
})