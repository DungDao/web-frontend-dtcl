export default {

    getChampions({ commit }, payload) {
        let url = "home/list-champions";
        return this.$axios
            .$get(url, { params: payload.params })
            .then(res => {
                return res;
            })
            .catch(error => {
                return error
            })
    },

    getChampionById({ commit }, payload) {
        let url = "home/champions/" + payload.id;
        return this.$axios.$get(url).then(res => {
            return res;
        }).catch(err => {
            return err;
        })
    }
}