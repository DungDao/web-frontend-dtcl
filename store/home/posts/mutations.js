export default {
    GET_LIST_CATEGORY(state, payload) {
        state.listCategory = payload
    },

    GET_LIST_NEW_POST(state, payload) {
        state.listNewPost = payload
    },

    GET_LIST_POPU_POST(state, payload) {
        state.listPopuPost = payload
    },

}