export default {
    loadListCate({ commit }, payload) {
        let url = 'home/get-categories';
        return this.$axios
            .$get(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    getNewPost({ commit }, payload) {
        let url = "home/get-post-new";
        this.$axios
            .$get(url)
            .then(res => {
                if (res.code == 200) {
                    commit('GET_LIST_NEW_POST', res.data);
                }
            })
            .catch(error => {
                return error
            })
    },

    getPopuPost({ commit }, payload) {
        let url = "home/get-post-popu";
        this.$axios
            .$get(url)
            .then(res => {
                if (res.code == 200) {
                    commit('GET_LIST_POPU_POST', res.data);
                }
            })
            .catch(error => {
                return error
            })
    },

    getPostById({ commit }, payload) {
        let url = "home/get-post/" + payload.id;
        return this.$axios.$get(url).then(res => {
            return res;
        }).catch(err => {
            return err;
        })
    },

    getPostByCategory({ commit }, payload) {
        let url = "home/get-post-by-cate-id/" + payload.id;
        return this.$axios.$get(url).then(res => {
            return res;
        }).catch(err => {
            return err;
        })
    },

    whatPost({ commit }, payload) {
        let url = "home/watch-post/" + payload.id;
        return this.$axios.$put(url).then(res => {
            return res;
        }).catch(err => {
            return err;
        })
    },

    // Comment Api

    addComment({ commit, dispatch }, payload) {
        let url = "home/comment"
        return this.$axios
            .$post(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },

    deleteComment({ commit, dispatch }, payload) {

        var url = 'home/comment/' + payload.id;
        return this.$axios
            .$delete(url)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
    updateComment({ commit }, payload) {
        var url = 'home/comment/' + payload.form.id
        return this.$axios
            .$put(url, payload.form)
            .then(res => {
                return res
            })
            .catch(error => {
                return error
            })
    },
}